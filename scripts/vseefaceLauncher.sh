#!/usr/bin/bash
cd ~/VseeFaceDir/
find . -name "GPUManagementPlugin.dll" -delete
cd OpenSeeFace
virtualenv -p python3 env
source env/bin/activate
nohup python facetracker.py -c 0 -W 1280 -H 720 --discard-after 0 --scan-every 0 --no-3d-adapt 1 --max-feature-updates 900 --ip 127.0.0.1 --port 11573 &
cd ../VSeeFace
wine VSeeFace.exe