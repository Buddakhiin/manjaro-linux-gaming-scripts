#!/usr/bin/bash
wget https://vd.clipstudio.net/clipcontent/paint/app/1132/CSP_1132w_setup.exe
winetricks -q win10
winetricks -q allcodecs
winetricks -q dxvk
winetricks -q vcrun2005
winetricks -q vcrun2008
winetricks -q vcrun2010
winetricks -q vcrun2012
winetricks -q vcrun2013
winetricks -q vcrun2015
winetricks -q vcrun2017
winetricks -q vcrun6
winetricks -q vcrun6sp6
winetricks -q msxml4
winetricks -q msxml6
winetricks -q mfc40
winetricks -q mfc42
winetricks -q fontfix
winetricks -q corefonts
winetricks -q allfonts
wine CSP_1132w_setup.exe
