#!/usr/bin/bash
sudo pacman -S python python-pip python-virtualenv git wine
winetricks vcrun2015 
winetricks fontfix
winetricks corefonts
winetricks allfonts
cd ~/
mkdir VseeFaceDir
cd VseeFaceDir
wget https://github.com/emilianavt/VSeeFaceReleases/releases/download/v1.13.38c/VSeeFace-v1.13.38c2.zip
unzip VSeeFace*.zip
rm -rf *.zip
find . -name "GPUManagementPlugin.dll" -delete
git clone https://github.com/emilianavt/OpenSeeFace
cd OpenSeeFace
virtualenv -p python3 env
source env/bin/activate
pip3 install onnxruntime opencv-python pillow numpy
